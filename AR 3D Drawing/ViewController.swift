//
//  ViewController.swift
//  AR 3D Drawing
//
//  Created by Anuj Dutt on 11/6/17.
//  Copyright © 2017 Anuj Dutt. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate, SCNPhysicsContactDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var draw: UIButton!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var refresh: UIBarButtonItem!
    @IBOutlet weak var save: UIBarButtonItem!
    @IBOutlet weak var load: UIBarButtonItem!
    @IBOutlet weak var tblDropDown: UITableView!
    @IBOutlet weak var tblDropDownHC: NSLayoutConstraint!
    @IBOutlet weak var btnNumberOfObject: UIButton!
    @IBOutlet weak var labelView: UILabel!
    @IBOutlet weak var labelViewObj: UILabel!

    

    
    private var newAngleY :Float = 0.0
    private var currentAngleY :Float = 0.0
    private var localTranslatePosition :CGPoint!
    
    private var isTap: Bool = true
    private var isEdit: Bool = false
    
    private var nodesX = [float_t]()
    private var nodesY = [float_t]()
    private var nodesZ = [float_t]()
    var nodeSpX = [float_t]()
    var nodeSpY = [float_t]()
    var nodeSpZ = [float_t]()

    private var loadedNodesX = [float_t]()
    private var loadedNodesY = [float_t]()
    private var loadedNodesZ = [float_t]()
    var loadedNodesSpX = [float_t]()
    var loadedNodesSpY = [float_t]()
    var loadedNodesSpZ = [float_t]()
    
    private var isLoadedNodeParentAvailable : Bool = false
    private var cameraPostion: SCNVector3 = SCNVector3(0, 0, 0)
    
    //Untuk menyimpan nilai posisi dari objek yang di load
    private var loadedNodePostion = [SCNVector3]()
    private var node1 = SCNVector3()
    private var node2 = SCNVector3()
    
    var sp = [SCNNode]()
    var isTableVisible = false
    var text = String()
    var chairNode = SCNNode()

    var objectList = ["JajarGenjang.dae","Kotak.dae","LayangLayang.dae","PersegiPanjang.dae","Segitiga.dae"]
    
    
    let config = ARWorldTrackingConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,ARSCNDebugOptions.showWorldOrigin]
        self.sceneView.showsStatistics = true
        self.sceneView.session.run(config)
        // Call the rendering function while the Scene is being rendered
        self.sceneView.delegate = self
        
        let scene = SCNScene()
        
        sceneView.scene = scene
        
        self.sceneView.scene.physicsWorld.contactDelegate = self
        registerGestureRecognizers()
        
        tblDropDown.delegate = self
        tblDropDown.dataSource = self
        tblDropDownHC.constant = 0
        
        
    }
    
    private func registerGestureRecognizers() {
        
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinched))
        self.sceneView.addGestureRecognizer(pinchGestureRecognizer)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panned))
        self.sceneView.addGestureRecognizer(panGestureRecognizer)
        
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector
            (longPressed))
        self.sceneView.addGestureRecognizer(longPressGestureRecognizer)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.sceneView.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// UI __________________________________________
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "numberofobject")
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: "numberofobject")
        }
        cell?.textLabel?.text = objectList[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        btnNumberOfObject.setTitle(objectList[indexPath.row], for: .normal)
        text = objectList[indexPath.row]
        UIView.animate(withDuration: 0.5){
            self.tblDropDownHC.constant = 0
            self.isTableVisible = false
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func selectObject(_ sender : AnyObject){
        
        UIView.animate(withDuration: 0.5){
            if self.isTableVisible == false {
                self.isTableVisible = true
                self.tblDropDownHC.constant = 44.0 * 3.0
            }else{
                self.tblDropDownHC.constant = 0
                self.isTableVisible = false
            }
            self.view.layoutIfNeeded()
            
        }
        
    }
    
// UI ---------------------------------
    
    
    @IBAction func editTapped(_ sender: UIButton) {
        if edit.titleLabel?.text == "Edit"{
            
            edit.setTitle("Done", for: .normal)
            isEdit = true
            draw.isHidden = true
            
            
        }else if edit.titleLabel?.text == "Done"{
            
            edit.setTitle("Edit", for: .normal)
            isEdit = false
            draw.isHidden = false
            
        }
    }
    
    
    @IBAction func refreshBarButton(_ sender: UIBarButtonItem) {
        sceneView.session.pause()
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            node.removeFromParentNode()
        }
        if let configuration = sceneView.session.configuration {
            sceneView.session.run(configuration,
                                  options: .resetTracking)
        }
        nodesX.removeAll()
        nodesY.removeAll()
        nodesZ.removeAll()
        // new
        loadedNodePostion.removeAll()
        sp.removeAll()
        isTap = true
        self.isLoadedNodeParentAvailable = false;
    }
    
// Spawn_______________________________
    
    @objc func tapped(recognizer :UITapGestureRecognizer) {
        
        if isTap == true {
            
            guard let seceneView = recognizer.view as? ARSCNView else {
                return
            }
            
            let touch = recognizer.location(in: seceneView)
            
            let hitTestResults = seceneView.hitTest(touch, options: nil)
            
            if let hitTest = hitTestResults.first {
                
                let layar = hitTest.node
                let chairScene = SCNScene(named: "Assets.scnassets/\(text)")!
                
                let chairSceneChildNodes = chairScene.rootNode.childNodes
                
                for childNode in chairSceneChildNodes {
                    childNode.physicsBody = SCNPhysicsBody(type: .kinematic, shape: nil)
                    childNode.physicsBody?.contactTestBitMask = 1
                    chairNode.addChildNode(childNode)
                    sp.append(childNode)
                    print(sp.count)
                }
                
                
                chairNode.position = cameraPostion
                
                nodeSpX.append(chairNode.position.x)
                nodeSpY.append(chairNode.position.y)
                nodeSpZ.append(chairNode.position.z)
                
                chairNode.name = "nodeParent"
                layar.addChildNode(chairNode)
                print("metu")
                
            }
            isTap = false
        }
    }
    

// Spawn------------------------------------^^^^^^
    
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        var numberCek = 0;
        while numberCek < sp.count {
            if (contact.nodeA == sp[numberCek] || contact.nodeA.name == "pointer") && (contact.nodeB == sp[numberCek] || contact.nodeB.name == "pointer"){
                sp[numberCek].geometry?.firstMaterial?.diffuse.contents = UIColor.green

            }
            numberCek += 1;
        }

    }
    
    func physicsWorld(_ world: SCNPhysicsWorld, didUpdate contact: SCNPhysicsContact) {
        var numberCek = 0;
        while numberCek < sp.count{
           
                sp[numberCek].geometry?.firstMaterial?.diffuse.contents = UIColor.white
            
            numberCek += 1;
        }
    }
    

    
    
    
    @objc func panned(recognizer :UIPanGestureRecognizer) {
        
        if isEdit == true{
            
            if recognizer.state == .changed {
                
                guard let sceneView = recognizer.view as? ARSCNView else {
                    return
                }
                
                let translation = recognizer.translation(in: sceneView)
                if chairNode.name == "nodeParent"{
                    
                    let cubeNode = chairNode
                    
                    self.newAngleY = Float(translation.x) * (Float) (Double.pi)/180
                    self.newAngleY += self.currentAngleY
                    cubeNode.eulerAngles.y = self.newAngleY
                }else if recognizer.state == .ended{
                    self.currentAngleY = self.newAngleY
            }
        }
    }
}
    
    @objc func pinched(recognizer :UIPinchGestureRecognizer) {
        
        if isEdit == true{
            
            if recognizer.state == .changed {
                
                if chairNode.name == "nodeParent"{
                    
                    let cubeNode = chairNode
                    
                    let pinchScaleX = Float(recognizer.scale) * cubeNode.scale.x
                    let pinchScaleY = Float(recognizer.scale) * cubeNode.scale.y
                    let pinchScaleZ = Float(recognizer.scale) * cubeNode.scale.z

                    cubeNode.scale = SCNVector3(pinchScaleX,pinchScaleY,pinchScaleZ)

                    recognizer.scale = 1
                    
                }
            }
        }
    }
    
    @objc func longPressed(recognizer :UILongPressGestureRecognizer) {
        
        if isEdit == true {
            
            guard let sceneView = recognizer.view as? ARSCNView else {
                return
            }
            let touch = recognizer.location(in: sceneView)
            if chairNode.name == "nodeParent"{
                if let parentNode = chairNode.parent{
                    if recognizer.state == .began {
                        localTranslatePosition = touch
                    } else if recognizer.state == .changed {

                        let deltaX = Float(touch.x - self.localTranslatePosition.x)/700
                        let deltaY = Float(touch.y - self.localTranslatePosition.y)/700

                        parentNode.localTranslate(by: SCNVector3(deltaX,0.0,deltaY))
                        self.localTranslatePosition = touch

                    }

                }
            }
        }
    }
    
    
    func Save(){

        do{
            //ini untuk serialize array
            let jsonDataX = try JSONSerialization.data(withJSONObject: nodesX, options: .prettyPrinted)
            let jsonDataY = try JSONSerialization.data(withJSONObject: nodesY, options: .prettyPrinted)
            let jsonDataZ = try JSONSerialization.data(withJSONObject: nodesZ, options: .prettyPrinted)
            let jsonDataSpX = try JSONSerialization.data(withJSONObject: nodeSpX, options: .prettyPrinted)
            let jsonDataSpY = try JSONSerialization.data(withJSONObject: nodeSpY, options: .prettyPrinted)
            let jsonDataSpZ = try JSONSerialization.data(withJSONObject: nodeSpZ, options: .prettyPrinted)
            //            print(jsonData)
            
            //ngubah hasil serialize ke string
            let strX = String(data: jsonDataX, encoding: .utf8)
            let strY = String(data: jsonDataY, encoding: .utf8)
            let strZ = String(data: jsonDataZ, encoding: .utf8)
            let strSpX = String(data: jsonDataSpX, encoding: .utf8)
            let strSpY = String(data: jsonDataSpY, encoding: .utf8)
            let strSpZ = String(data: jsonDataSpZ, encoding: .utf8)
            
            let fileNameX = getDocumentsDirectory().appendingPathComponent("outputX.txt")
            let fileNameY = getDocumentsDirectory().appendingPathComponent("outputY.txt")
            let fileNameZ = getDocumentsDirectory().appendingPathComponent("outputZ.txt")
            let fileNameSpX = getDocumentsDirectory().appendingPathComponent("outputSpX.txt")
            let fileNameSpY = getDocumentsDirectory().appendingPathComponent("outputSpY.txt")
            let fileNameSpZ = getDocumentsDirectory().appendingPathComponent("outputSpZ.txt")
            
            
            print("letak file bos : \(fileNameX)")
            
            //menyimpan di drive dalam bentuk .txt
            try strX?.write(to: fileNameX, atomically: true, encoding: String.Encoding.utf8)
            try strY?.write(to: fileNameY, atomically: true, encoding: String.Encoding.utf8)
            try strZ?.write(to: fileNameZ, atomically: true, encoding: String.Encoding.utf8)
            try strSpX?.write(to: fileNameSpX, atomically: true, encoding: String.Encoding.utf8)
            try strSpY?.write(to: fileNameSpY, atomically: true, encoding: String.Encoding.utf8)
            try strSpZ?.write(to: fileNameSpZ, atomically: true, encoding: String.Encoding.utf8)
            
            
        }catch
        {
            //handle error
            print(error)
        }
    }
    
    //ini untuk mendapatkan letak folder
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getDocumentsDirectory2() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    @IBAction func saveButton(_ sender: UIBarButtonItem) {
        self.Save()
        
//        let filename = getDocumentsDirectory2().appendingPathComponent("outputX.txt")
//
//        let fileURL = NSURL(fileURLWithPath: filename)
//
//        let objectsToShare = [fileURL]
//
//        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//
//        self.present(activityVC, animated: true, completion: nil)

    }
    
    func Load()
    {
        
        let fileNameX = getDocumentsDirectory().appendingPathComponent("outputX.txt")
        let fileNameY = getDocumentsDirectory().appendingPathComponent("outputY.txt")
        let fileNameZ = getDocumentsDirectory().appendingPathComponent("outputZ.txt")
        let fileNameSpX = getDocumentsDirectory().appendingPathComponent("outputSpX.txt")
        let fileNameSpY = getDocumentsDirectory().appendingPathComponent("outputSpY.txt")
        let fileNameSpZ = getDocumentsDirectory().appendingPathComponent("outputSpZ.txt")
        
        do
        {
            //ini untuk load data
            let textX = try String(contentsOf: fileNameX, encoding: .utf8)
            let textY = try String(contentsOf: fileNameY, encoding: .utf8)
            let textZ = try String(contentsOf: fileNameZ, encoding: .utf8)
            let textSpX = try String(contentsOf: fileNameSpX, encoding: .utf8)
            let textSpY = try String(contentsOf: fileNameSpY, encoding: .utf8)
            let textSpZ = try String(contentsOf: fileNameSpZ, encoding: .utf8)
            
            let dataX = Data(textX.utf8)
            let dataY = Data(textY.utf8)
            let dataZ = Data(textZ.utf8)
            let dataSpX = Data(textSpX.utf8)
            let dataSpY = Data(textSpY.utf8)
            let dataSpZ = Data(textSpZ.utf8)
            
            loadedNodesX = try JSONDecoder().decode([float_t].self, from: dataX)
            loadedNodesY = try JSONDecoder().decode([float_t].self, from: dataY)
            loadedNodesZ = try JSONDecoder().decode([float_t].self, from: dataZ)
            loadedNodesSpX = try JSONDecoder().decode([float_t].self, from: dataSpX)
            loadedNodesSpY = try JSONDecoder().decode([float_t].self, from: dataSpY)
            loadedNodesSpZ = try JSONDecoder().decode([float_t].self, from: dataSpZ)
            
            
            
            print("Loaded Node X count : \(loadedNodesX.count)")
            print("Loaded Node Y count : \(loadedNodesY.count)")
            print("Loaded Node Z count : \(loadedNodesZ.count)")
            
            
            CreateLoadedParent()

            var number = 0;
            while number < loadedNodesX.count {
                let sphereNode  = SCNNode(geometry: SCNSphere(radius: 0.02))
    
                sphereNode.name = "loadedNode"
                // Put Sphere node in camera view
                sphereNode.position = SCNVector3(x: loadedNodesX[number], y: loadedNodesY[number], z: loadedNodesZ[number])
                self.sceneView.scene.rootNode.enumerateChildNodes({(node,_) in
                    if node.name == "loadedNodeParent"{
                        node.addChildNode(sphereNode)
                    }
                })
                sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
                
        
                number+=1;
            }
            var number2 = 0;
            while number2 < loadedNodesSpX.count{
                let chairScene = SCNScene(named: "Assets.scnassets/\(text)")!
                
                let chairSceneChildNodes = chairScene.rootNode.childNodes
                
                for childNode in chairSceneChildNodes {
                    childNode.physicsBody = SCNPhysicsBody(type: .kinematic, shape: nil)
                    childNode.physicsBody?.contactTestBitMask = 1
                    chairNode.addChildNode(childNode)
                    sp.append(childNode)
                    print(sp.count)
                }
                
                self.sceneView.scene.rootNode.enumerateChildNodes({(node2,_) in
                    if node2.name == "loadedNodeParent"{
                        node2.addChildNode(chairNode)
                    }
                })
                chairNode.position = SCNVector3(x: loadedNodesX[number2], y: loadedNodesY[number2], z: loadedNodesZ[number2])
                number2 += 1;
            }
            
        }catch
        {
            
        }
    }
    
    
    @IBAction func loadButton(_ sender: UIBarButtonItem) {
        Load()
    }
    
    func CreateLoadedParent() {
        let loadedNodeParent = SCNNode(geometry: SCNSphere(radius: 0.01))
        loadedNodeParent.name = "loadedNodeParent"
        self.sceneView.scene.rootNode.addChildNode(loadedNodeParent)
        loadedNodeParent.position = cameraPostion
        loadedNodeParent.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
        labelViewObj.text = String(format: "X: %.4f\nY: %.4f\nZ: %.4f", loadedNodeParent.position.x, loadedNodeParent.position.y, loadedNodeParent.position.z)
        print("Created")
    }
    

    
    
    func renderer(_ renderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: TimeInterval) {
        
        guard let pointOfView = sceneView.pointOfView else {return}
        
        let transform = pointOfView.transform
        
        
        let cameraOrientation = SCNVector3(-transform.m31,-transform.m32,-transform.m33)
        
        let cameraLocation = SCNVector3(transform.m41,transform.m42,transform.m43)
        
        let cameraCurrentPosition = cameraOrientation + cameraLocation
        DispatchQueue.main.async {
            
            self.cameraPostion = cameraCurrentPosition
            
            // Check if Button is pressed or not and draw content
            if self.draw.isHighlighted{
                let sphereNode  = SCNNode(geometry: SCNSphere(radius: 0.01))
                // Put Sphere node in camera view
                sphereNode.name = "pen"
                sphereNode.position = cameraCurrentPosition
                sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
                sphereNode.physicsBody = SCNPhysicsBody(type: .kinematic, shape: nil)
                sphereNode.physicsBody?.contactTestBitMask = 1
                
                self.sceneView.scene.rootNode.addChildNode(sphereNode)
                print("Draw Button is Pressed")
                self.nodesX.append(sphereNode.position.x)
                self.nodesY.append(sphereNode.position.y)
                self.nodesZ.append(sphereNode.position.z)
            }
                // if person is not drawing, place a pointer in that location
            else{
                let pointer = SCNNode(geometry: SCNSphere(radius: 0.01))
                pointer.name = "pointer"
                // Put pointer in current position of camera
                pointer.position = cameraCurrentPosition
                // before adding the new pointer to scrreen, delete every old pointer
                // else it forms a line
                self.sceneView.scene.rootNode.enumerateChildNodes({(node,_) in
                    if node.name == "pointer"{
                        node.removeFromParentNode()
                    }
                })
                
                self.labelView.text = String(format: "X: %.2f\nY: %.2f\nZ: %.2f", pointer.position.x, pointer.position.y, pointer.position.z)
                
                pointer.physicsBody = SCNPhysicsBody(type: .kinematic, shape: nil)
                pointer.physicsBody?.contactTestBitMask = 1
                self.sceneView.scene.rootNode.addChildNode(pointer)
                pointer.geometry?.firstMaterial?.diffuse.contents = UIColor.red
            }
            
        }
    }
}

func +(left:SCNVector3,right:SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x + right.x, left.y + right.y, left.z + right.z)
}


